CREATE TABLE autores
(
    id_autor         SERIAL PRIMARY KEY,
    nombre           VARCHAR(255) NOT NULL,
    fecha_nacimiento DATE,
    created_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP, -- Nuevo
    updated_at       TIMESTAMP DEFAULT CURRENT_TIMESTAMP  -- Nuevo
);

CREATE TABLE libros
(
    id_libro   SERIAL PRIMARY KEY,
    titulo     VARCHAR(255) NOT NULL,
    id_autor   INTEGER REFERENCES autores (id_autor),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, -- Nuevo
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP  -- Nuevo
);


INSERT INTO autores (nombre, fecha_nacimiento)
VALUES ('Gabriel García Márquez', '1927-03-06'),
       ('Isabel Allende', '1942-08-02');


INSERT INTO libros (titulo, id_autor)
VALUES ('Cien años de soledad', 1),
       ('El amor en los tiempos del cólera', 1),
       ('La casa de los espíritus', 2);