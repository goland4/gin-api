package model

import (
	"github.com/go-playground/validator/v10"
	"time"
)

type Autores struct {
	IDAutor         uint       `gorm:"primaryKey"`
	Nombre          string     `gorm:"not null;size:255" validate:"required,min=2,max=255"`
	FechaNacimiento *time.Time `gorm:"column:fecha_nacimiento"`
	CreatedAt       time.Time  `gorm:"column:created_at"`
	UpdatedAt       time.Time  `gorm:"column:updated_at"`
}

// Método para validar el modelo
func (a *Autores) Validate() error {
	validate := validator.New() // Se crea una instancia de validador
	return validate.Struct(a)   // Se valida la estructura
}
