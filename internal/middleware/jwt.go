package middleware

import (
	"api/internal/handler"
	"github.com/gin-gonic/gin"
	"net/http"
)

func JWTMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := validateToken(c)
		if err != nil { // Si hay un error, devolverlo
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		c.Next()
	}
}

func validateToken(c *gin.Context) error {
	authHandler := handler.NewAuthHandler()
	return authHandler.ValidateToken(c)
}
