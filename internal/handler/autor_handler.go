package handler

import (
	"api/internal/model"
	"api/internal/repository"
	"github.com/gin-gonic/gin"
	"strconv"
)

type AutorHandler struct {
	autorRepo repository.AutorRepository
}

// NewAutorHandler esta función crea un nuevo AutorHandler que recibe un repositorio de autores
func NewAutorHandler(autorRepo repository.AutorRepository) *AutorHandler {
	return &AutorHandler{autorRepo}
}

func (ah *AutorHandler) CrearAutor(c *gin.Context) {
	var autor model.Autores

	//validar el JSON recibido
	if err := c.ShouldBindJSON(&autor); err != nil { //c.ShouldBindJSON(&autor) es una función de Gin que mapea el JSON recibido a la estructura autor
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	//validar el modelo Autor
	if err := autor.Validate(); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	//crear el autor llamando al repositorio
	if err := ah.autorRepo.Create(&autor); err != nil {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	//retornar el autor creado con el código 201
	c.JSON(201, autor)

}

func (ah *AutorHandler) ObtenerAutorPorID(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.ParseUint(idStr, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{"error": "ID inválido"})
		return
	}

	autor, err := ah.autorRepo.GetByID(uint(id))
	if err != nil {
		c.JSON(404, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, autor)
}

func (ah *AutorHandler) ObterAutorJoin(c *gin.Context) {
	autores, err := ah.autorRepo.GetAllJoin()
	if err != nil {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, autores)
}

func (ah *AutorHandler) ObtenerTodosLosAutores(c *gin.Context) {
	autores, err := ah.autorRepo.GetAll()
	if err != nil {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, autores)
}

func (ah *AutorHandler) ActualizarAutor(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.ParseUint(idStr, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{"error": "ID inválido"})
		return
	}

	var autor model.Autores
	if err := c.ShouldBindJSON(&autor); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	if err := ah.autorRepo.Update(uint(id), &autor); err != nil {
		c.JSON(500, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, autor)
}
