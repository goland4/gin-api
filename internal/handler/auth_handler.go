package handler

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"net/http"
	"os"
	"strings"
	"time"
)

type AuthHandler struct{}

// Constructor
func NewAuthHandler() *AuthHandler {
	return &AuthHandler{}
}

func (ac *AuthHandler) GenerateToken(c *gin.Context) {
	// Crea el token JWT
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp": time.Now().Add(time.Hour * 24).Unix(), // Expira en 24 horas
	})

	// Firma el token con tu clave secreta
	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error al generar el token"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": tokenString})
}

func (ac *AuthHandler) ValidateToken(c *gin.Context) error {
	// Obtener el token de la cabecera Authorization
	authHeader := c.GetHeader("Authorization")
	tokenString := strings.TrimPrefix(authHeader, "Bearer ")

	// Parsear y validar el token
	err := ac.validateJWTToken(tokenString)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "EEEEHHHH Maquinola quien chota sos?, token inválido"})
	}
	return nil
}

func (ac *AuthHandler) validateJWTToken(tokenString string) error {
	//
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("método de firma inválido")
		}
		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	if err != nil || !token.Valid {
		return errors.New("token inválido")
	}
	return nil
}
