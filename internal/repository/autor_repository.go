package repository

import (
	"api/internal/model"
	"gorm.io/gorm"
)

// esto es como si fuera una clase
type AutorRepository interface {
	Create(autor *model.Autores) error
	GetByID(id uint) (*model.Autores, error)
	GetAll() ([]*model.Autores, error)
	GetAllJoin() ([]map[string]interface{}, error)
	Update(id uint, autor *model.Autores) error
	Delete(id uint) error
}

// se define la estructura autorRepository
type autorRepository struct {
	db *gorm.DB
}

// Funcion que crea una nueva instancia de la clase autorRepository
func NewAutorRepository(db *gorm.DB) AutorRepository {
	return &autorRepository{db}
}

//////////////Funciones para el CRUD////////////////////

func (r *autorRepository) Create(autor *model.Autores) error {
	// Crea un autor en la base de datos
	return r.db.Create(autor).Error
}

func (r *autorRepository) GetByID(id uint) (*model.Autores, error) {

	// Crea una variable autor del tipo model.Autores, esto almacenará el autor encontrado
	var autor model.Autores

	// Obtiene un autor por su ID
	err := r.db.First(&autor, id).Error
	return &autor, err
}

func (r *autorRepository) GetAll() ([]*model.Autores, error) {
	// Crea un slice de autores
	var autores []*model.Autores //almacenara todos los autores encontrados

	// Obtiene todos los autores
	err := r.db.Find(&autores).Error //le paso autores para que me devuelva todos los autores
	return autores, err
}

func (r *autorRepository) GetAllJoin() ([]map[string]interface{}, error) {
	// Crea un slice de mapas de interfaces
	var results []map[string]interface{}

	// Ejecuta la consulta SQL personalizada
	result := r.db.Raw("SELECT * FROM libros JOIN public.autores a ON a.id_autor = libros.id_autor").Scan(&results)
	if result.Error != nil {
		return nil, result.Error
	}

	return results, nil
}

func (r *autorRepository) Update(id uint, autor *model.Autores) error {
	// Actualiza un autor por su ID
	return r.db.Model(&model.Autores{}).Where("id_autor = ?", id).Updates(autor).Error

}

func (r *autorRepository) Delete(id uint) error {
	// Elimina un autor por su ID
	return r.db.Delete(&model.Autores{}, id).Error
}
