package routes

import (
	"api/internal/handler"
	"api/internal/middleware"
	"api/internal/repository"
	"github.com/gin-gonic/gin"
)

func AutorRoutes(router *gin.Engine, autorRepo repository.AutorRepository) {

	// Crear una nueva instancia de AutorHandler, que se refiere al repositorio de autores
	autorHandler := handler.NewAutorHandler(autorRepo)

	// Crear grupo de rutas para autores
	autorRouters := router.Group("/api/autores")

	{
		autorRouters.POST("/", autorHandler.CrearAutor)
		autorRouters.GET("/", autorHandler.ObtenerTodosLosAutores)
		autorRouters.GET("/:id", autorHandler.ObtenerAutorPorID)
		autorRouters.GET("/join", middleware.JWTMiddleware(), autorHandler.ObterAutorJoin)
	}

}
