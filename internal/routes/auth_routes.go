package routes

import (
	"api/internal/handler"
	"github.com/gin-gonic/gin"
)

func AuthRoutes(router *gin.Engine) {

	// Crear una nueva instancia de AuthHandler
	authHandler := handler.NewAuthHandler()

	// Crear grupo de rutas
	authGroup := router.Group("/auth")

	{
		authGroup.GET("/token", authHandler.GenerateToken)
	}
}
