package main

import (
	"api/internal/config"
	"api/internal/repository"
	"api/internal/routes"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"log"
)

func main() {
	// Configuración de la base de datos
	db := config.InitDB()

	//inicializar el repositorio de autores
	autorRepo := repository.NewAutorRepository(db)

	// Obtener la conexión SQL subyacente
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalf("Error al obtener la conexión SQL: %v", err)
	}
	defer sqlDB.Close() // Cerrar la conexión al finalizar

	// Crear instancia de Gin
	r := gin.Default()

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Configurar rutas de autores
	routes.AutorRoutes(r, autorRepo)
	routes.AuthRoutes(r)

	// Iniciar el servidor en el puerto 8080
	if err := r.Run(":8080"); err != nil {
		log.Fatalf("Error al iniciar el servidor: %v", err)
	}
}
